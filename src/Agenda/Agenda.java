package Agenda;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.Vector;

public class Agenda {

    private static int totalPessoas;
    
    private static int agendaIdade;
    private static String agendaNomes;
    private static double agendaAltura;
    
    static Vector<String> setNome = new Vector<String>();
    static Vector<Integer> setIdade = new Vector<Integer>();
    static Vector<Double> setAltura = new Vector<Double>();
    
    static Scanner teclado = new Scanner(System.in);

    public void quantidadePessoas(int qtdade) {
    	totalPessoas = qtdade;
    }
    
    public static void armazenaPessoa(int i) {
        System.out.println("Digite o nome da " + (i + 1) + " pessoa");
        teclado = new Scanner(System.in);
        agendaNomes = teclado.nextLine();
        System.out.println("Digite a idade da " + (i + 1) + " pessoa");
        teclado = new Scanner(System.in);
        agendaIdade = teclado.nextInt();
        System.out.println("Digite a altura da " + (i + 1) + " pessoa");
        teclado = new Scanner(System.in);
        String altura = teclado.nextLine();
        agendaAltura = Double.parseDouble(altura);

        setNome.add(i, agendaNomes);
        setIdade.add(i, agendaIdade);
        setAltura.add(i, agendaAltura);
    }

    public static void imprimeAgenda(int index) {
        System.out.println((index + 1) + " ) Nome: " + setNome.get(index));
        System.out.println("Idade: " + setIdade.get(index));
        System.out.println("Altura: " + setAltura.get(index));
    }

    public static void removePessoa(int index) {
    	setNome.remove(index);
    	setIdade.remove(index);
    	setAltura.remove(index);
    }

    public static int buscaPessoa(String nomeBuscar) {
        int position = -1;
        for (int i = 0; i < totalPessoas; i++) {
            if (setNome.get(i).equals(nomeBuscar)) {
                position = i;
            }
        }
        return position;
    }
    
    
    public static void main(String[] args) {
	    Agenda ag = new Agenda();
	    int qtdadePessoas = 0;
	    int index;
	    String nome;
	    byte opcao = 10;

	    do {
	    	System.out.println("Escolha uma op��o:");
	    	System.out.println("1 - Cadastrar pessoas");
	    	System.out.println("2 - Buscar pessoas por nome");
	    	System.out.println("3 - Remover pessoas");
	    	System.out.println("4 - Imprimir agenda");
	    	System.out.println("5 - Imprimir pessoa pelo �ndice");
	    	System.out.println("0 - Sair");
	    	opcao = teclado.nextByte();
	    	try {
				switch (opcao) {
				case 1:
					System.out.println("Quantas pessoas quer cadastrar?");
					teclado = new Scanner(System.in);
					qtdadePessoas = teclado.nextInt();
					ag.quantidadePessoas(qtdadePessoas);
					for (int i = 0; i < qtdadePessoas; i++) {
						Agenda.armazenaPessoa(i);
					}
					break;
				case 2:
					System.out.println("Digite o nome da pessoa para buscar");
					teclado = new Scanner(System.in);
					nome = teclado.nextLine();
					int position = Agenda.buscaPessoa(nome);
					if (position >= 0) {
					    System.out.println(nome + " se encontra na posi��o " + (position + 1));
						for (int i = 0; i < qtdadePessoas; i++) {
						    if (i == position) {
						        Agenda.imprimeAgenda(i);
						    }
						}
					} else {
					    System.out.println("N�o encontramos pessoas com esse nome");
					}
					break;
				case 3:
					System.out.println("Digite o �ndice da pessoa que deseja remover");
					teclado = new Scanner(System.in);
					index = teclado.nextInt();
					for (int i = 0; i < qtdadePessoas; i++) {
					    if (i == (index - 1)) {
					        Agenda.removePessoa(i);
					    }
					}
					break;
				case 4:
					for (int i = 0; i < qtdadePessoas; i++) {
					    Agenda.imprimeAgenda(i);
					}
					break;
				case 5:
					System.out.println("Digite o �ndice da pessoa que deseja imprimir");
					teclado = new Scanner(System.in);
					index = teclado.nextInt();
					for (int i = 0; i < qtdadePessoas; i++) {
					    if (i == (index - 1)) {
					        Agenda.imprimeAgenda(i);
					    }
					}
					break;
				default:
					break;
				}
			} catch (InputMismatchException e) {
				System.out.println("Valor informado inv�lido");
			} catch (ArrayIndexOutOfBoundsException e) {
		    	System.out.println("Ind�ce removido");
		    } catch (Exception e) {
		    	System.out.println(e.getMessage());
		    }
	    } while (opcao != 0);
	}
}